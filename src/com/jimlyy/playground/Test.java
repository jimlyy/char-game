package com.jimlyy.playground;

public class Test {
	int steps = 10;
	int grow = 3;
	
	public Test(){
		int index = 0;
		
		for(int k =0; k < grow; k++){
			
			for(int i =  index; i < index + steps; i++){
				
				for(int j = 0; j < i; j++){
					System.out.print('+');
				}

				if(i > 0)
					System.out.println("");
			}
				index += 2;
		}
	}
	
	public static void main(String[] args) {
		new Test();
	}

}
