package com.jimlyy.playground.gridfighter.gameobjects;

import com.jimlyy.playground.gridfighter.Board;

public abstract class CombatantObjects {
	
	public String name = "Player 1";
	public static char nick = 'P';
	public final String Tombstone = "Player killed";
	
	private boolean ObjAlive = true;
	private int hp = 100;
	private int ap = 100;
	private int stepsToMove = 1;
	
	//Ev random stat cretated whit obj
	//public char xyStart = int randX = (int) (Math.random() * gridMaxX);
	public int xPos = 0;
	public int yPos = 0;
	public static int numOfPlayers = 0;
	
	public CombatantObjects() {
		
	}
	//Getters
	public int getHp(){
		return hp;
	}
	public int getAp(){
		return ap;
	}
	public int getMove(){
		return stepsToMove;
	}
	public char getNick(){
		return nick;
	}

	//Setters
	public void setHP(int hp){
		this.hp = hp;
	}
	public void setAp(int ap){
		this.ap = ap;
	}
	public void setAlive(boolean ObjAlive){
		this.ObjAlive = ObjAlive;
	}
	
	
	public void location(){
		
	}
}
